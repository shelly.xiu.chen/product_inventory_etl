# Product_inventory_ETL

Article type: Project
### Product ETL

You will be designing an ETL with a simple function to query products from a database.

You have been given ~5000 archived files in json.gz in the aws s3 directory s3://flashfood-engineering-assessment-data/ff-2020-03-dat-eng/


Bucket: flashfood-engineering-assessment-data
Folder: ff-2020-03-dat-eng


AWS Credentials 
HIDDEN


### Data Format
```
ff__zxixKBqIVebLKPl2a5aolfcTQ0suv9of--store_mapping_docs__2020-03-09 16:34:00.562761.json.gz
ff__zxwkVkbX2u3ySi2cuEej9Icu46AaalcJ--store_mapping_docs__2020-03-09 16:34:24.224019.json.gz
ff__zy5Juf9ZM3zIyusZ3tMkDZZ7HveMrmUW--store_mapping_docs__2020-03-09 16:30:47.004512.json.gz
```

### File Content
```
 [{
	"upc": "00012914720141",
	"name": "beans",
	"category": "Salad Bar-Short Cuts",
	"store_number": "1009",
	"price": 82.49657920027535,
	"description": "sudarium spatchcock preimpair trizone",
	"taxable": false,
	"department": "NOT STOCKED",
	"image": "https://i.picsum.photos/id/154231/400/400.jpg"
}, {
	"upc": "00013413903011",
	"name": "minable",
	"category": "Personal Care-Foot Care",
	"store_number": "1008",
	"price": 22.386698562221273,
	"description": "Cyrillianism cobbra jawbreaker isogamete",
	"taxable": false,
	"department": "HBC-Vitamins",
	"image": "https://i.picsum.photos/id/718121/400/400.jpg"
},
...]
```

### Specifications

We will run an automated test suite against the URL you supply, so please stick to the specifications as best as you can.


    Extract the data into any database of your choice. 
    Choose between document or relational databases. Explain your decision.
    You can also use a managed ETL service of your choice - Explain your decision.
    If you require additional permissions on the s3 bucket for your ETL, copy the data into a custom cloud storage and grant permissions as required. 
    Remove all duplicate records - use UPC, STORE_NUMBER, DEPARTMENT, CATEGORY, NAME as compound key
    Remove invalid data for price, upc, store_number
    Your solution should be reproducible - document every step.
    Write a custom function to query items by upc and store_number
    Leading zeros in upcs can be ignored. As an example upc 00012914720141 can be matched by any of these [ 0012914720141, 12914720141, 012914720141]
    For the purposes of this test, you can also ignore check digits i.e. the last digit of the UPC. As an example upc 00012914720141 can be matched by any of these [ 001291472014, 1291472014, 01291472014]. However an exact match of the searched upc should take precedence over a partial match
    Efficiency is very important


### Function Design

 

Design a function that takes upc:String and store_number: String and returns an Object | null


### Function get product

    Pass In: upc: String

    Pass In: store_number: String

    Call: query product

    Pass Out: product: Object

Endfunction


### Bonus Points


Only do this if you have some time left.


    Develop a simple API with a GET product endpoint to query products. The API should use the function you designed to query product.
    If the upc & store_number combination is not found, return HTTP response code 400 (Bad Request)


### > Example

If I run curl YOUR_URL?upc=<upc>&store_number=<store_number>

should return a product that matches that upc and store_number


### > Deploying

You can deploy anywhere on the public internet that's convenient for you. We want to be able to test your code by hitting an API, so whatever way makes most sense for you to make that possible works for us. If you're not sure where to deploy here are a couple of good choices:

    Heroku is a solid choice that should get you up and running quickly and for free, and has plenty of documentation on how to get started (https://devcenter.heroku.com/start), and sample projects which you can clone and modify (feel free to use these!) If you haven't used Heroku before, we recommend creating a Heroku account and reading the getting-started instructions before you start the timed portion of the challenge.
    You can run the code locally on the computer you developed it on, and set up an ngrok tunnel. You will run your code locally, and then run the ngrok program on your computer, and configure ngrok to forward requests to your public facing URL to your computer. This only works if your computer will be plugged in and running before our conversation so we can test!


### Return To Us

    A copy of your code (a github link is prefered, or a tarball file)
    Connection string to your database
    After you've returned it and we've had a chance to take a look, we'll call for ~30 minutes for a quick conversation about your code, a chance for you to ask questions, and some shared debugging work if one of the tests fails (so you’ll need to make sure you can access your development environment during the conversation, and we may ask you to screen share using the Google Hangouts chat).


### Values

    You can use whatever data lake, data warehouse, database (relational or document), ETL services, libraries and tools you like for your ETL pipeline
    What matters most is that you are able to extract the data in the most efficient way possible and perform queries through your function or api (if you decide to do the bonus).
    In the same vein, it is more important that your function does not query the wrong product.
    You should write code that you would want to work with and maintain in the future. We don't expect everything to be perfect, but if I can't read the code and understand what's going on, that's a problem.


### Advice

    Use tools that you’re comfortable with, as they’ll give you the best chance to get the work done.
    Deploy early and often; it will be much harder for us to judge your work if we can't hit your api to run it
    Pay attention to efficiency of your ETL and optimization of your database
    Pay attention to the function details, we can move onto debugging more interesting problems more quickly if we don’t have to adapt our test suite to your solution.